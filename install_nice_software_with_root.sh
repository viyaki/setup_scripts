#!/bin/bash
sudo add-apt-repository -y ppa:neovim-ppa/stable &&\
sudo apt-get update &&\
sudo apt-get upgrade -y &&\
sudo apt-get install -y \
    zsh \
    neovim
