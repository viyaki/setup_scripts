#!/bin/bash
export SINGVERSION=3.11.1
export SINGURL=https://github.com/sylabs/singularity/releases/download/v${SINGVERSION}/singularity-ce-${SINGVERSION}.tar.gz
wget -qO- $SINGURL | tar xzvf - &&\
cd singularity-ce-${SINGVERSION} &&\
./mconfig &&\
cd builddir &&\
make &&\
sudo make install &&\
. etc/bash_completion.d/singularity &&\
sudo cp etc/bash_completion.d/singularity /etc/bash_completion.d/

