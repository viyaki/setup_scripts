#!/bin/bash
export GOVERSION=1.20.2 OS=linux ARCH=arm64 &&\
wget https://go.dev/dl/go${GOVERSION}.${OS}-${ARCH}.tar.gz
sudo rm -rf /usr/local/go && sudo tar -C /usr/local -xzf go${GOVERSION}.${OS}-${ARCH}.tar.gz
rm go${GOVERSION}.${OS}-${ARCH}.tar.gz
export PATH=$PATH:/usr/local/go/bin
echo 'export PATH=/usr/local/go/bin:$PATH' >> $HOME/.bashrc &&\
echo 'export PATH=/usr/local/go/bin:$PATH' >> $HOME/.zshrc &&\
mkdir $HOME/go
mkdir $HOME/go/bin
curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh
