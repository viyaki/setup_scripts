#!/bin/bash
git clone https://github.com/ohmyzsh/ohmyzsh.git ${HOME}/.oh-my-zsh
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
git clone https://github.com/softmoth/zsh-vim-mode.git ${HOME}/.oh-my-zsh/plugins/zsh-vim-mode
SHELL=`which zsh`
/bin/bash $HOME/setup_scripts/install_shell_integration_and_utilities.sh
source $HOME/.iterm2_shell_integration.zsh
