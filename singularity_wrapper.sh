#!/bin/sh
export PATH=/usr/local/go/bin:$PATH
$HOME/setup_scripts/install_singularity_dependencies.sh
$HOME/setup_scripts/install_go.sh
$HOME/setup_scripts/install_singularity.sh
