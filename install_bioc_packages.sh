#!/bin/bash
mkdir -p $HOME/setup_scripts/r_bioc_pkgs
cat $HOME/setup_scripts/bioc_package_list.txt | parallel wget {} -P $HOME/setup_scripts/r_bioc_pkgs
$HOME/miniconda3/bin/conda run -n renv R CMD INSTALL r_bioc_pkgs/*gz -l $HOME/miniconda3/envs/renv/lib/R/library --use-vanilla
