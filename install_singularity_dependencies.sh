#!/bin/bash
sudo apt-get update &&\
sudo apt-get upgrade -y &&\
sudo apt-get install -y \
    build-essential \
    libssl-dev \
    uuid-dev \
    libgpgme11-dev \
    squashfs-tools \
    libseccomp-dev \
    wget \
    debootstrap \
    pkg-config \
    git \
    cryptsetup \
    libglib2.0-dev
